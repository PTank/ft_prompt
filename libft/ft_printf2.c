/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/14 07:57:22 by mcizo             #+#    #+#             */
/*   Updated: 2014/02/22 17:25:36 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void		print_char(t_param *printf_arg, int *len)
{
	ft_putchar((printf_arg->value.c));
	*len += 1;
}

void		print_integer(t_param *printf_arg, int *len)
{
	char	*number;

	number = ft_itoa(printf_arg->value.i);
	ft_putstr(number);
	*len += ft_strlen(number);
	if (printf_arg->value.i != -2147483648LL)
		free(number);
}

void		print_u_int(t_param *printf_arg, int *len)
{
	char	*number;

	number = ft_uitoa(printf_arg->value.u);
	ft_putstr(number);
	*len += ft_strlen(number);
}

void		print_str(t_param *printf_arg, int *len)
{
	if (printf_arg->value.str == NULL)
		printf_arg->value.str = "(null)";
	ft_putstr(printf_arg->value.str);
	*len += ft_strlen(printf_arg->value.str);
}

void		print_format(const char *format, int *index, int *len)
{
	ft_putchar(format[*index]);
	*index += 1;
	*len += 1;
}
