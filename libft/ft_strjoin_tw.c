/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin_tw.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gponsine <gponsine@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/02 04:50:11 by gponsine          #+#    #+#             */
/*   Updated: 2014/03/02 04:54:59 by gponsine         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin_tw(char *s1, char *s2, char *s3)
{
	char	*join;
	int		i;
	int		j;

	join = ft_strnew(ft_strlen(s1) + ft_strlen(s2) + ft_strlen(s3));
	i = j = 0;
	while (s1[i])
		join[j++] = s1[i++];
	i = 0;
	while (s2[i])
		join[j++] = s2[i++];
	i = 0;
	while (s3[i])
		join[j++] = s3[i++];
	return (join);
}
