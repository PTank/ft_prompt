/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf3.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/22 00:20:20 by mcizo             #+#    #+#             */
/*   Updated: 2014/02/22 17:25:50 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void	print_oct_int(t_param *printf_param, int *len)
{
	char			*result;
	unsigned int	mod;

	result = ft_strnew(0);
	while (printf_param->value.u >= 8)
	{
		mod = printf_param->value.u % 8;
		printf_param->value.u = (printf_param->value.u - mod) / 8;
		result = ft_strjoin(ft_itoa(mod), result);
	}
	result = ft_strjoin(ft_itoa(printf_param->value.u), result);
	ft_putstr(result);
	*len += ft_strlen(result);
	free(result);
}
