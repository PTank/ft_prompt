/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_uitoa.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/21 21:39:45 by mcizo             #+#    #+#             */
/*   Updated: 2014/01/14 04:26:27 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char	*strrevcpy(char *str)
{
	int					i;
	size_t				len;
	char				*revcpy;

	i = 0;
	len = ft_strlen(str);
	revcpy = ft_strnew(len);
	while (len > 0)
	{
		revcpy[i] = str[len - 1];
		len--;
		i++;
	}
	revcpy[i] = '\0';
	return (revcpy);
}

static int	digit_counter(unsigned int n)
{
	int					len;

	len = 0;
	while (n)
	{
		n /= 10;
		len++;
	}
	return (len);
}

static char	*return_zero(void)
{
	char				*ret;

	ret = ft_strnew(1);
	ret[0] = '0';
	return (ret);
}

char		*ft_uitoa(unsigned int n)
{
	unsigned int	result;
	int				i;
	char			*tmp;
	int				len;

	i = 0;
	result = n;
	if (n > 0)
	{
		len = digit_counter(n);
		tmp = ft_strnew(len);
		while (result)
		{
			tmp[i] = (result % 10) + 48;
			result = result / 10;
			i++;
		}
		return (strrevcpy(tmp));
	}
	if (n == 0)
		return (return_zero());
	else
		return (NULL);
}
