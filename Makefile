# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: akazian <akazian@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/12/16 16:00:35 by akazian           #+#    #+#              #
#    Updated: 2014/03/13 11:48:36 by akazian          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME		=	ft_prompt
CC			=	gcc
CFLAGS		=	-Wall -Wextra -Werror -O3 -g3 -pedantic
LIBFTDIR	=	./libft/
LIBFTH		=	-I$(LIBFTDIR)
LIBFTFLAGS	=	-L$(LIBFTDIR) -lft -ltermcap
INCS_DIR	=	includes
OBJS_DIR	=	objects
SRCS_DIR	=	sources
MAIN_DIR	=	.
MAIN		=	main.c
SRCS		=	ft_init_termcap.c\
			ft_init_history.c\
			ft_open_history.c\
			ft_struct_history.c\
			ft_getenv.c\
			ft_init_cap.c\
			ft_xtgetstr.c\
			ft_open_term.c\
			ft_add_to_buff.c\
			ft_catch_enter.c\
			ft_catch_key.c\
			ft_del_char.c\
			ft_get_cap.c\
			ft_no_cap.c\
			ft_erease_line.c\
			ft_add_old_str.c\
			ft_key_arrow.c\
			ft_save_history.c\
			ft_reset_term.c\
			ft_put_term.c\
			ft_output.c\
			ft_control_term.c\
			ft_get_term_size.c\
			ft_putbuff.c\
			ft_handle_resize.c

OBJS 		=	$(patsubst %.c, $(OBJS_DIR)/%.o, $(MAIN))
OBJS 		+=	$(patsubst %.c, $(OBJS_DIR)/%.o, $(SRCS))

all		:	$(NAME)

$(NAME)		:	$(OBJS_DIR) $(OBJS)
	$(CC) -o $(NAME) $(OBJS) $(LIBFTFLAGS)

$(OBJS_DIR)/%.o	:	$(addprefix $(MAIN_DIR)/, %.c)
	$(CC) $(CFLAGS) -o $@ -c $^ -I $(INCS_DIR) $(LIBFTH)
$(OBJS_DIR)/%.o	:	$(addprefix $(SRCS_DIR)/, %.c)
	$(CC) $(CFLAGS) -o $@ -c $^ -I $(INCS_DIR) $(LIBFTH)

$(OBJS_DIR)	:	makelibft
	mkdir -p $(OBJS_DIR)

makelibft:
	make -C $(LIBFTDIR)

fclean		:	clean
	rm -f $(NAME)

clean		:
	rm -rf $(OBJS_DIR)

re		:	fclean all

.PHONY: clean all re fclean
