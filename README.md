#FT_PROMPT

It's a soft shell prompt with termcap lib.

##Lib

    $> sudo apt-get install libncurses5-dev libncursesw5-dev

##Man

###	Include

    <include> ft_termcap.h

###	Function init

####ft_init_history(t_shell *s)
####ft_init_termcap(t_shell *s)

	t_shell s;
	extern 	char **environ;

	s.prompt = "our_prompt";
	s.prompt_size = len(s.prompt);
	s.history_limit = 100; /* auto limit to 10 000 */
	s.env = environ;
	ft_init_history(&s);
	ft_init_termcap(&s);

###	Function get line

####ft_get_cap(t_shell *s, int quote_mode)

    char *line;
	line = ft_get_cap(&s, FALSE); /*work with no termcap*/

TRUE or FALSE to use "quote_mode" in TRUE ft_get_cap don't save line in history and don't free return at end

* Control d: if line clean return exit * No work on quote mode *
* Control l: clear window
* Control a: move curser the first char
* Control e: move curser to the last char

return:
A string or NULL

###	Function save and free allocated blocks

####ft_save_history(t_shell *s)

    ft_save_history(&s);

####	Author

akazian
