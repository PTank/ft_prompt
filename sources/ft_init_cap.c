/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_cap.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/03 11:03:32 by akazian           #+#    #+#             */
/*   Updated: 2014/03/17 14:51:45 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_termcap.h>

int	ft_init_cap(t_shell *s)
{
	char	t[4096];

	s->term->area = t;
	s->term->clean = ft_xtgetstr("cl", &s->term->area);
	s->term->cmch = ft_xtgetstr("ch", &s->term->area);
	s->term->cmup = ft_xtgetstr("up", &s->term->area);
	s->term->delcd = ft_xtgetstr("cd", &s->term->area);
	return (0);
}
