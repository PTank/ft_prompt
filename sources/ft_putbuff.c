/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putbuff.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/13 12:46:16 by akazian           #+#    #+#             */
/*   Updated: 2014/03/17 14:51:45 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_termcap.h>
#include <term.h>
#include <libft.h>


int	ft_putbuff(t_shell *s, int pos)
{
	int	len;
	int	line;
	int	line_pos;
	int	up;

	ft_putstr(s->history->buffer);
	if (!pos)
		return (0);
	up = 0;
	len = ft_strlen(s->history->buffer);
	line = (len + s->prompt_size) / s->term->nx;
	line_pos = (len + s->prompt_size - pos) / s->term->nx;
	if (len + s->prompt_size - pos < s->term->nx)
		up = line;
	else
		up = line - line_pos;
	while (up)
	{
		tputs(s->term->cmup, 1, ft_output);
		up--;
	}
	tputs(tgoto(s->term->cmch, 0, (len + s->prompt_size - pos)
				% s->term->nx), 1, ft_output);
	return (0);
}
