/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_add_to_buff.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/04 17:39:53 by akazian           #+#    #+#             */
/*   Updated: 2014/03/17 14:51:44 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_termcap.h>
#include <libft.h>


int	ft_add_to_buff(t_shell *s, char *buf, int pos)
{
	int		len;
	char	*new_buffer;
	int		i;
	int		j;

	len = (s->history->buffer) ? ft_strlen(s->history->buffer) + 1 : 1;
	new_buffer = ft_strnew(len);
	i = j = 0;
	pos = len - (pos + 1);
	while (i != len)
	{
		if (i == pos)
			new_buffer[i] = buf[0];
		else
		{
			new_buffer[i] = s->history->buffer[j];
			j++;
		}
		i++;
	}
	ft_strdel(&s->history->buffer);
	s->history->buffer = new_buffer;
	return (0);
}
