/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_erease_line.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/07 19:18:39 by akazian           #+#    #+#             */
/*   Updated: 2014/03/17 14:51:45 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <term.h>
#include <ft_termcap.h>
#include <libft.h>

int	ft_erease_line(int pos, int len, t_shell *s)
{
	int	line_pos;

	ft_putchar(' ');
	if (s->prompt_size + len >= s->term->x)
	{
		line_pos = (len + s->prompt_size - pos) / s->term->x;
		while (line_pos)
		{
			tputs(s->term->cmup, 1, ft_output);
			line_pos--;
		}
	}
	tputs(tgoto(s->term->cmch, 0, 0), 1, ft_output);
	tputs(s->term->delcd, 1, ft_output);
	ft_putstr(s->prompt);
	return (0);
}
