/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_output.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/13 10:30:04 by akazian           #+#    #+#             */
/*   Updated: 2014/03/13 10:36:52 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <libft.h>
#include <fcntl.h>

int	ft_output(int c)
{
	static int	fd = 0;

	if (fd == 0 && isatty(STDIN_FILENO) != 0)
		fd = open(ttyname(STDIN_FILENO), O_RDWR);
	ft_putchar_fd(c, fd);
	return (0);
}
