/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getenv.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/24 14:13:02 by akazian           #+#    #+#             */
/*   Updated: 2014/03/03 11:22:17 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_getenv(const char **envp, char *word)
{
	char	*keyword;
	char	*find;
	int		i;
	int		len;

	i = 0;
	find = NULL;
	if (word)
	{
		keyword = ft_strjoin(word, "=");
		len = ft_strlen(keyword);
		while (envp && envp[i])
		{
			if (ft_strncmp(envp[i], keyword, len) == 0)
				find = (char *)envp[i] + len;
			i++;
		}
	}
	ft_strdel(&keyword);
	return (find);
}
