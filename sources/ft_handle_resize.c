/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_handle_resize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/17 11:16:30 by akazian           #+#    #+#             */
/*   Updated: 2014/03/17 14:51:45 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <signal.h>
#include <sys/ioctl.h>
#include <ft_termcap.h>
#include <unistd.h>
#include <term.h>
#include <libft.h>

static t_shell	*ft_charge_struct(t_shell *s)
{
	static t_shell	*setup = NULL;

	if (s)
		setup = s;
	return (setup);
}

static void		ft_resize(int sig)
{
	t_shell			*s;
	struct winsize	size;

	(void)sig;
	s = ft_charge_struct(NULL);
	ioctl(STDIN_FILENO, TIOCGWINSZ, (char *) &size);
	s->term->y = size.ws_row;
	s->term->x = size.ws_col;
	ioctl(0, TIOCSTI, s->term->new_term);
}

int				ft_signal_resize(t_shell *s)
{
	s->term->y = s->term->ny;
	s->term->x = s->term->nx;
	ft_charge_struct(s);
	signal(SIGWINCH, ft_resize);
	return (1);
}
