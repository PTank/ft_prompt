/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_cap.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/03 12:15:35 by akazian           #+#    #+#             */
/*   Updated: 2014/03/17 14:51:45 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <libft.h>
#include <ft_termcap.h>

static char		*ft_return_value(t_shell *s, int quote_mode, int ret)
{
	char	*value;

	ft_putchar('\n');
	value = s->history->buffer;
	if (ret != 2)
		return (NULL);
	if (!quote_mode)
	{
		if (s->history->next)
		{
			while (s->history->buffer && s->history->next)
				s->history = s->history->next;
			s->history->buffer = ft_strdup(value);
		}
		add_history_elem(&s->history, s->history, NULL);
	}
	else
		s->history->buffer = NULL;
	return (value);
}

char			*ft_get_cap(t_shell *s, int quote_mode)
{
	int		len;
	int		pos;
	char	buff[5] = {0};
	int		ret;

	if (ft_put_term(s))
		return (ft_no_caps(s, quote_mode));
	pos = 0;
	while (ft_control_term(s, buff, quote_mode, &pos))
	{
		len = 0;
		ft_bzero(buff, 5);
		if (s->history->buffer && (len = ft_strlen(s->history->buffer)))
			ft_putbuff(s, pos);
		if (ft_signal_resize(s) && (read(0, buff, 5)) < 0)
			return (NULL);
		if ((ret = ft_catch_enter(s, buff)) && ft_reset_term(s))
			return (ft_return_value(s, quote_mode, ret));
		pos = ft_catch_key(s, buff, pos, len);
	}
	return ("exit");
}
