/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_termcap.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/28 14:36:40 by akazian           #+#    #+#             */
/*   Updated: 2014/03/17 14:51:45 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_termcap.h>
#include <unistd.h>
#include <stdlib.h>

int	ft_init_termcap(t_shell *s)
{
	s->term = NULL;
	if (ft_open_term(s))
		ft_init_cap(s);
	else
	{
		if (s->term)
			free(s->term);
		s->term = NULL;
	}
	return (0);
}
