/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_del_char.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/04 17:32:42 by akazian           #+#    #+#             */
/*   Updated: 2014/03/17 14:51:45 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_termcap.h>
#include <libft.h>

int			ft_del_char(t_shell *s, int pos)
{
	int		len;
	char	*new_buffer;

	len = ft_strlen(s->history->buffer);
	pos = len - pos - 1;
	if (len == 1)
	{
		ft_strdel(&s->history->buffer);
		s->history->buffer = NULL;
		return (0);
	}
	new_buffer = ft_strnew(len);
	ft_add_old_str(pos, len, s->history->buffer, new_buffer);
	ft_strdel(&s->history->buffer);
	s->history->buffer = new_buffer;
	return (0);
}
