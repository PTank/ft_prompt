/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_catch_key.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/04 11:43:17 by akazian           #+#    #+#             */
/*   Updated: 2014/03/17 14:51:45 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_termcap.h>
#include <libft.h>

int	ft_catch_key(t_shell *s, char *buff, int pos, int len)
{
	int	key;

	key = *(unsigned int *) buff;
	ft_erease_line(pos, len, s);
	if (key == UP)
		pos = ft_up(s, pos);
	else if (key == DOWN)
		pos = ft_down(s, pos);
	else if (key == RIGHT)
		pos += ft_right(pos);
	else if (key == LEFT)
		pos += ft_left(pos, len);
	else if (key == BACKSPACE)
	{
		if (s->history->buffer)
			ft_del_char(s, pos);
	}
	else if (ft_isprint(buff[0]))
		ft_add_to_buff(s, buff, pos);
	return (pos);
}
