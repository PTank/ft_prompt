/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_save_history.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/11 16:51:57 by akazian           #+#    #+#             */
/*   Updated: 2014/03/17 14:51:45 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_termcap.h>
#include <unistd.h>
#include <fcntl.h>
#include <libft.h>
#include <stdlib.h>

static int	ft_free_termcap(t_shell *s)
{
	if (s->term)
	{
		free(s->term->new_term);
		free(s->term->save_term);
		free(s->term);
	}
	return (0);
}

static int	ft_safe_limit(t_shell *s)
{
	if (s->history_limit > 10000 || s->history_limit < 0)
		s->history_limit = 500;
	return (1);
}

int			ft_save_history(t_shell *s)
{
	char	save_file[] = ".42sh_history";
	int		fd;
	int		i;
	int		j;

	i = j = 0;
	if ((fd = open(save_file, O_RDWR | O_CREAT | O_TRUNC, S_IRUSR
					| S_IWUSR)) < 0)
		return (0);
	ft_safe_limit(s);
	while (s->history->back && s->history->back->buffer && ++i)
		s->history = s->history->back;
	while (s->history && s->history->buffer && ++j)
	{
		if (i - j < s->history_limit)
			ft_putendl_fd(s->history->buffer, fd);
		ft_strdel(&s->history->buffer);
		s->history = s->history->next;
		if (s->history)
			free(s->history->back);
	}
	ft_strdel(&s->history->buffer);
	free(s->history);
	ft_free_termcap(s);
	return (0);
}
