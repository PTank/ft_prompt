/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_control_term.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/13 11:01:33 by akazian           #+#    #+#             */
/*   Updated: 2014/03/17 14:51:45 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_termcap.h>
#include <term.h>
#include <libft.h>

int		ft_control_term(t_shell *s, char *buff, int quote_mode, int *pos)
{
	if (!quote_mode && !s->history->buffer && buff[0] == 4 && ft_reset_term(s))
		return (0);
	if (buff[0] == 12)
	{
		tputs(s->term->clean, 1, ft_output);
		ft_putstr(s->prompt);
	}
	if (buff[0] == 1 && s->history->buffer)
		*pos = ft_strlen(s->history->buffer);
	if (buff[0] == 5)
		*pos = 0;
	return (1);
}
