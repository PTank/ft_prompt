/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_catch_enter.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/04 11:23:34 by akazian           #+#    #+#             */
/*   Updated: 2014/03/17 14:51:45 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_termcap.h>
#include <libft.h>

int	ft_catch_enter(t_shell *s, char *buf)
{
	int	key;

	key = *(unsigned int *)buf;
	if (key == RETURN)
	{
		if (s->history->buffer)
			return (2);
		return (1);
	}
	return (0);
}
