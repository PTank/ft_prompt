/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_key_arrow.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/11 15:46:33 by akazian           #+#    #+#             */
/*   Updated: 2014/03/17 14:51:45 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_termcap.h>

int		ft_up(t_shell *s, int pos)
{
	if (s->history->back)
	{
		s->history = s->history->back;
		return (0);
	}
	return (pos);
}

int		ft_down(t_shell *s, int pos)
{
	if (s->history->next)
	{
		s->history = s->history->next;
		return (0);
	}
	return (pos);
}

int		ft_right(int pos)
{
	if (pos > 0)
		return (-1);
	return (0);
}

int		ft_left(int pos, int len)
{
	if (pos < len)
		return (1);
	return (0);
}
