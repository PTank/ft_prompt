/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_open_history.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/28 14:36:48 by akazian           #+#    #+#             */
/*   Updated: 2014/03/17 14:51:45 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_termcap.h>
#include <unistd.h>
#include <libft.h>
#include <fcntl.h>

int	ft_open_history(t_shell *s)
{
	char		file[] = ".42sh_history";
	int			fd;
	char		*line;
	int			first;

	first = 0;
	if ((fd = open(file, O_RDONLY)) < 0)
		return (0);
	while ((get_next_line(fd, &line)) > 0)
	{
		if (line[0] != '\0')
		{
			if (first == 0)
				add_history_elem(&s->history, NULL, line);
			else
				add_history_elem(&s->history, s->history, line);
			first = 1;
		}
		line = NULL;
	}
	if (s->history->buffer)
		add_history_elem(&s->history, s->history, NULL);
	close(fd);
	return (0);
}
