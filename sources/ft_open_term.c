/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_open_term.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/28 15:10:25 by akazian           #+#    #+#             */
/*   Updated: 2014/03/17 14:51:45 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_termcap.h>
#include <term.h>
#include <libft.h>

int	ft_open_term(t_shell *s)
{
	char			*term_name;
	char			bp[1024];

	if (!(s->term = ft_memalloc(sizeof(t_term))))
		return (0);
	if (!(term_name = ft_getenv((const char **)s->env, "TERM")))
		return (0);
	if (!(s->term->new_term = ft_memalloc(sizeof(struct termios))))
		return (0);
	if (!(s->term->save_term = ft_memalloc(sizeof(struct termios))))
		return (0);
	if (tgetent(bp, term_name) != 1)
		return (0);
	tcgetattr(STDIN_FILENO, s->term->new_term);
	tcgetattr(STDIN_FILENO, s->term->save_term);
	s->term->new_term->c_lflag &= ~(ICANON | ECHO);
	s->term->new_term->c_cc[VMIN] = 1;
	s->term->new_term->c_cc[VTIME] = 0;
	ft_get_term_size(s);
	return (1);
}
