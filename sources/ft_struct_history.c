/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_struct_history.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/28 14:36:59 by akazian           #+#    #+#             */
/*   Updated: 2014/03/11 11:55:33 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_termcap.h>
#include <libft.h>

t_history	*new_history_elem(void)
{
	t_history	*new;

	if ((new = (t_history *)ft_memalloc(sizeof(t_history))) == NULL)
		return (NULL);
	new->next = NULL;
	new->back = NULL;
	new->buffer = NULL;
	return (new);
}

void		add_history_elem(t_history **h, t_history *back, char *buffer)
{
	t_history	*new;

	if (!(*h)->buffer)
		new = *h;
	else
		new = new_history_elem();
	new->back = back;
	new->buffer = buffer;
	(*h)->next = new;
	(*h) = new;
	new->next = NULL;
}
