/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_xtgetstr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/03 11:10:08 by akazian           #+#    #+#             */
/*   Updated: 2014/03/03 11:23:50 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_termcap.h>
#include <libft.h>
#include <term.h>

char	*ft_xtgetstr(char *id, char **area)
{
	char	*cap;

	if (!(cap = tgetstr(id, area)))
	{
		ft_putstr_fd("termcap:", 2);
		ft_putstr_fd(id, 2);
		ft_putstr_fd(": error", 2);
	}
	return (cap);
}
