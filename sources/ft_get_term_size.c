/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_term_size.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/13 11:21:37 by akazian           #+#    #+#             */
/*   Updated: 2014/03/17 14:51:45 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_termcap.h>
#include <sys/ioctl.h>

int	ft_get_term_size(t_shell *s)
{
	struct winsize	size;

	if (ioctl(STDIN_FILENO, TIOCGWINSZ, (char*) &size) < 0)
		return (1);
	s->term->y = size.ws_row;
	s->term->x = size.ws_col;
	s->term->ny = s->term->y;
	s->term->nx = s->term->x;
	return (0);
}
