/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_no_cap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/04 10:13:59 by akazian           #+#    #+#             */
/*   Updated: 2014/03/17 14:51:45 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <ft_termcap.h>

char	*ft_no_caps(t_shell *s, int quote_mode)
{
	char	*line;

	line = NULL;
	if ((get_next_line(0, &line)) >= 0)
	{
		if (line && !quote_mode)
		{
			s->history->buffer = line;
			add_history_elem(&s->history, s->history, NULL);
		}
	}
	return (line);
}
