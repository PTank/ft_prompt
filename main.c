/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/28 14:54:25 by akazian           #+#    #+#             */
/*   Updated: 2014/03/17 14:51:45 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <ft_termcap.h>
#include <stdlib.h>

int	main(void)
{
	t_shell		s;
	extern char **environ;
	char		*line;

	s.history = NULL;
	s.env = environ;
	ft_init_history(&s);
	ft_init_termcap(&s);
	s.history_limit = 20;
	s.prompt = ft_strdup("prompt> ");
	s.prompt_size = ft_strlen(s.prompt);
	while (42)
	{
		ft_putstr(s.prompt);
		line = ft_get_cap(&s, FALSE);
		if (line)
		{
			ft_printf("%s\n",line);
			if (ft_strcmp(line, "exit") == 0)
				break ;
		}
	}
	ft_save_history(&s);
	free(s.prompt);
	return (0);
}
