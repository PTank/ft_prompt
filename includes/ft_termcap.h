/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcap.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/28 14:36:17 by akazian           #+#    #+#             */
/*   Updated: 2014/03/17 14:51:45 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef _FT_TERMCAP_H
# define _FT_TERMCAP_H

# include <termios.h>

# define STDIN_FILENO 0
# define TRUE 1
# define FALSE 0

# define UP 4283163
# define DOWN 4348699
# define RIGHT 4414235
# define LEFT 4479771
# define BACKSPACE 127
# define DEL 2117294875
# define RETURN 10

typedef struct			s_history
{
	struct s_history	*next;
	struct s_history	*back;
	char				*buffer;
}						t_history;

typedef struct			s_term
{
	struct termios		*save_term;
	struct termios		*new_term;
	int					x;
	int					y;
	int					nx;
	int					ny;
	char				*clean;
	char				*area;
	char				*cmch;
	char				*cmup;
	char				*delcd;
	int					pos;
	int					len;
}						t_term;

typedef struct			s_shell
{
	t_term				*term;
	t_history			*history;
	int					history_limit;
	char				**env;
	int					prompt_size;
	char				*prompt;
}						t_shell;

int			ft_add_to_buff(t_shell *s, char *buf, int pos);
int			ft_catch_enter(t_shell *s, char *buf);
int			ft_catch_key(t_shell *s, char *buff, int posi, int len);
int			ft_del_char(t_shell *s, int pos);
char		*ft_get_cap(t_shell *s, int quote_mode);
char		*ft_no_caps(t_shell *s, int quote_mode);
int			ft_erease_line(int pos, int len, t_shell *s);
void		ft_add_old_str(int pos, int len, char *src, char *str);
int			ft_up(t_shell *s, int pos);
int			ft_down(t_shell *s, int pos);
int			ft_right(int pos);
int			ft_left(int pos, int len);
int			ft_control_term(t_shell *s, char *buff, int quote_mode, int *pos);
int			ft_putbuff(t_shell *s, int pos);
int			ft_signal_resize(t_shell *s);

/* termcap */
int			ft_init_termcap(t_shell *s);
int			ft_open_term(t_shell *s);
char		*ft_xtgetstr(char *id, char **area);
int			ft_init_cap(t_shell *s);
int			ft_put_term(t_shell *s);
int			ft_reset_term(t_shell *s);
int			ft_output(int c);
int			ft_get_term_size(t_shell *s);

/* history */
t_history	*new_history_elem(void);
void		add_history_elem(t_history **h, t_history *back, char *buffer);
int			ft_init_history(t_shell *s);
int			ft_open_history(t_shell *s);
int			ft_save_history(t_shell *s);

/* Common func */
char		*ft_getenv(const char **envp, char *word);

#endif
